<?php
/******************************************************************************
* Title: functions.php
* Author: Hunter Schoonover ~ http://hunterschoonover.com
* Date: 02/17/18
*
* Theme functions, supports, and definitions.
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package zzzYourThemeNameHere
*
*/

//add featured image support to theme
add_theme_support('post-thumbnails');

//--------------------------------------------------------------------------
// ::zzzYourThemeNameHere_addStylesAndScripts
//
// Ensures that WordPress knows to load all of the necessary files and 
// scripts necessary to this theme.
//

add_action('wp_enqueue_scripts', 'zzzYourThemeNameHere_addStylesAndScripts');

function zzzYourThemeNameHere_addStylesAndScripts() 
{
    
    wp_enqueue_style('normalize', get_template_directory_uri() . '/normalize.css');
    wp_enqueue_style('subtle', get_stylesheet_uri());
    
}// end of ::zzzYourThemeNameHere_addStylesAndScripts
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// ::zzzYourThemeNameHere_disableUpdateRequest
//
// Disable requests to wp.org repository for this theme.
// 
// If this is not included, naming conflicts may occur. WordPress 
// automatically searches wp.org for a theme with the same name as yours.
// If one is found, WordPress will consider your theme and the one found to 
// be the same. It will then attempt to update your theme using the latest 
// version of the theme it found.
//
// Original code from:
//      https://wptheming.com/2014/06/disable-theme-update-checks/
//

add_filter('http_request_args', 'zzzYourThemeNameHere_disableUpdateRequest', 10, 2 );

function zzzYourThemeNameHere_disableUpdateRequest($pResponse, $pUrl) 
{

	// If it's not a theme update request, bail.
	if (0 !== strpos($pUrl, 'https://api.wordpress.org/themes/update-check')) {
        return $pResponse;
    }

    // Decode the JSON response
    $themes = json_decode($pResponse['body']['themes'] );

    // Remove the active parent and child themes from the check
    $parent = get_option('template');
    $child = get_option('stylesheet');
    unset($themes->themes->$parent);
    unset($themes->themes->$child);

    // Encode the updated JSON response
    $pResponse['body']['themes'] = json_encode($themes);

    return $pResponse;
    
}// end of ::zzzYourThemeNameHere_disableUpdateRequest
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// ::zzzYourThemeNameHere_getFeaturedImagePath
//
// Gets and returns the absolute source path for the current post's featured 
// image, ensuring it fits into $pWidth and $pHeight.
//
// @params
//      $pWidth     width image must fit in px. Default = 500px
//      $pHeight    height image must fit in px. Default = 500px
//
// @return
//      returns absolute image source path.
//      Example: "http://site.com/image.jpg"
//

function zzzYourThemeNameHere_getFeaturedImagePath($pWidth=500, $pHeight=500)
{
    
    return wp_get_attachment_image_src(get_post_thumbnail_id(), 
                                        array($pWidth,$pHeight))[0];
    
}// end of ::zzzYourThemeNameHere_getFeaturedImagePath
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// ::zzzYourThemeNameHere_getPostHref
//
// Gets and returns the current page's href.
//
// @params
//      none
//
// @return
//      returns href of current page.
//      Example: "http://site.com/page/"
//

function zzzYourThemeNameHere_getPostHref()
{
    
    return esc_url(get_permalink());
    
}// end of ::zzzYourThemeNameHere_getPostHref
//--------------------------------------------------------------------------

?>