<?php
/******************************************************************************
* Title: single.php
* Author: Hunter Schoonover ~ http://hunterschoonover.com
* Date: 02/17/18
*
* The template for displaying all single posts.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package zzzYourThemeNameHere
*
*/
?>

<?php get_header(); ?>

    <?php
    while ( have_posts() ) : the_post();

        get_template_part( 'template-parts/content', 'single' );

        the_post_navigation();

    endwhile; // End of the loop.
    ?>

<?php get_footer(); ?>
