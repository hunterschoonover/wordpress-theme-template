<?php
/******************************************************************************
* Title: content-page.php
* Author: Hunter Schoonover ~ http://hunterschoonover.com
* Date: 02/17/18
*
* The template part for displaying page content in page.php.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package zzzYourThemeNameHere
*
*/
?>

         <div id="post-<?php the_ID(); ?>">

            <!-- Title -->
            <a href="<?php echo zzzYourThemeNameHere_getPostHref(); ?>"><?php echo get_the_title(); ?></a>
            <br>
            <?php if (has_post_thumbnail()) { ?>

                <!-- Featured Image -->
                <img src="<?php echo zzzYourThemeNameHere_getFeaturedImagePath(500, 500); ?>" alt="">

            <?php } ?>   
            <br>
            <!-- Author -->
            <?php echo get_the_author(); ?>
            <br>
            <!-- Date -->
            <?php echo get_the_date('F j, Y'); ?>

        </div><!-- #post-### -->