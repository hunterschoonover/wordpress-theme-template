<?php
/******************************************************************************
* Title: 404.php
* Author: Hunter Schoonover ~ http://hunterschoonover.com
* Date: 02/17/18
*
* The template for displaying 404 pages (not found).
*
* @link https://codex.wordpress.org/Creating_an_Error_404_Page
*
* @package zzzYourThemeNameHere
*
*/
?>

<?php get_header(); ?>

        <div id="post-<?php the_ID(); ?>">

            <!-- Title -->
            Oops! You broke the internet!

        </div>

<?php get_footer(); ?>
