<?php
/******************************************************************************
* Title: header.php
* Author: Hunter Schoonover ~ http://hunterschoonover.com
* Date: 02/17/18
*
* The template for displaying the pages header.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package zzzYourThemeNameHere
*
*/
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    
    <title><?php bloginfo('name'); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="totop">
	
    <!-- HEADER SECTION
    ================================================================================================
    ================================================================================================ -->
    <div id="zzzYourThemeNameHere_headerSection">

        <a href="/"><img src="/HunterSchoonoverLogo.jpg" alt="Hunter Schoonover"></a>

        <ul>
            <li><a href="/subscribe/">subscribe</a></li>
            <li><a href="/about/">about</a></li>
            <li><a href="/contact/">contact</a></li>
        </ul>

    </div>
    <!-- end HEADER SECTION
    ================================================================================================
    ================================================================================================ -->

    <!-- CONTENT SECTION
    ================================================================================================
    ================================================================================================ -->
    <div id="zzzYourThemeNameHere_contentSection">
